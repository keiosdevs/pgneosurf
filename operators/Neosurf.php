<?php namespace Keios\PGNeosurf\Operators;

use Finite\StatefulInterface;
use Keios\PaymentGateway\Support\HashIdsHelper;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PGNeosurf\Classes\NeosurfMerchant;
use Keios\PGNeosurf\Classes\NeosurfMerchantReceive;
use Keios\PGNeosurf\Classes\NeosurfMerchantSend;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PaymentGateway\Core\Operator;

/**
 * Class Neosurf
 *
 * @package Keios\PGNeosurf
 */
class Neosurf extends Operator implements StatefulInterface
{
    use SettingsDependent;

    const CREDIT_CARD_REQUIRED = false;

    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgneosurf::lang.operators.neosurf';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgneosurf/assets/img/neosurf/logo.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    /**
     *
     * @throws \Exception
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {
        $chargeMaker = new NeosurfMerchantSend();

        $internalRedirect = \URL::to(
            '_paymentgateway/'.
            OperatorUrlizer::urlize($this).
            '?pgUuid='.base64_encode($this->uuid)
        );

        try {
            $returnUrl = $chargeMaker->send($this->cart, $this->uuid, $internalRedirect);
        } catch (\Exception $e) {
            throw new \Exception($e);
//            return new PaymentResponse($this, null, [$e->getMessage()]);
        }

        return new PaymentResponse($this, $returnUrl);
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processNotification(array $data)
    {
        \Log::info(print_r($data, true));

        $notificationParser = new NeosurfMerchantReceive();

        if(!array_key_exists('rep',$data)){
             return \Redirect::to($this->returnUrl);
        }

        $isPaid = $notificationParser->receive($data);

        if(array_key_exists('mode', $data)){
            \Response::json(['status' => 'ok'], 200);
        }

        if ($isPaid && $this->can(Operator::TRANSITION_ACCEPT)) {
            try {
                $this->accept();
                return \Response::json(['status' => 'ok'], 200);

            } catch (\Exception $ex) {
                \Log::error($ex->getMessage());
                return \Redirect::to($this->returnUrl);
            }

        } else {
            return \Redirect::to($this->returnUrl);
        }
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {
        // todo if possible

        return new PaymentResponse($this, null);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function extractUuid(array $data)
    {
        if (isset($data['pgUuid'])) {
            return base64_decode($data['pgUuid']);
        } else {
            throw new \RuntimeException('Invalid redirect, payment uuid is missing.');
        }
    }

}
