<?php

return [
    'labels'    => [
        'pluginDesc' => 'Neosurf Integration Extension for Keios PaymentGateway',
    ],
    'operators' => [
        'neosurf' => 'Neosurf',
    ],
    'settings'  => [
        'tab'        => 'Neosurf',
        'general'    => 'General settings',
        'apiKey'     => 'You Neosurf Secret Key',
        'merchantId' => 'Neosurf Merchant ID',
        'serviceId'  => 'Neosurf Service ID',
        'testMode'   => 'Use test mode',
    ],
    'info'      => [
        'header' => 'How to retrieve your Neosurf api key',
    ],
];