<?php namespace Keios\PGNeosurf;

use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use System\Classes\PluginBase;
use Event;

/**
 * PG-PGNeosurf Plugin Information File
 *
 * @package Keios\PGNeosurf
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PG-Neosurf',
            'description' => 'keios.pgneosurf::lang.labels.pluginDesc',
            'author'      => 'Keios',
            'icon'        => 'icon-neosurf'
        ];
    }

    public function register()
    {
        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGNeosurf\Operators\Neosurf');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'neosurf.general'  => [
                            'label' => 'keios.pgneosurf::lang.settings.general',
                            'tab'   => 'keios.pgneosurf::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'neosurf.info'     => [
                            'type' => 'partial',
                            'path' => '$/keios/pgneosurf/partials/_neosurf_info.htm',
                            'tab'  => 'keios.pgneosurf::lang.settings.tab',
                        ],
                        'neosurf.apiKey'   => [
                            'label'   => 'keios.pgneosurf::lang.settings.apiKey',
                            'tab'     => 'keios.pgneosurf::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'neosurf.merchantId'   => [
                            'label'   => 'keios.pgneosurf::lang.settings.merchantId',
                            'tab'     => 'keios.pgneosurf::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'neosurf.serviceId'   => [
                            'label'   => 'keios.pgneosurf::lang.settings.serviceId',
                            'tab'     => 'keios.pgneosurf::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '1'
                        ],
                        'neosurf.testMode' => [
                            'label' => 'keios.pgneosurf::lang.settings.testMode',
                            'tab'   => 'keios.pgneosurf::lang.settings.tab',
                            'type'  => 'switch'
                        ],
                    ]
                );
            }
        );
    }
}
