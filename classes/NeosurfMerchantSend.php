<?php namespace Keios\PGNeosurf\Classes;


use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PaymentGateway\ValueObjects\Cart;
use Validator;

/**
 * Class NeosurfMerchantSend
 * @package Keios\PGNeosurf\Classes
 */
class NeosurfMerchantSend
{
    use SettingsDependent;
    /**
     * @var NeosurfMerchant
     */
    private $merchant;

    /**
     * NeosurfMerchantSend constructor.
     */
    public function __construct()
    {
        $this->merchant = new \Keios\PGNeosurf\Classes\NeosurfMerchant();
    }


    /**
     * @param Orderable $cart
     * @param string    $uuid
     * @param string    $returnUrl
     *
     * @return string
     * @throws \Exception
     */
    public function send($cart, $uuid, $returnUrl)
    {
        $this->getSettings();
        $currency = $cart->getTotalGrossCost()->getCurrency()->getIsoCode();

        $data = [];
        $data['IDMerchant'] = $this->getSettings()->get('neosurf.merchantId');
        $data['IDService'] = $this->getSettings()->get('neosurf.serviceId');
        $data['URLConfirmation'] = $returnUrl . '?mode=confirm';
        $data['IDTransaction'] = $uuid;
        $data['amount'] = $cart->getTotalGrossCost()->getAmountBasic();
        $data['Type'] = '1;currency=' . $currency . ';';
        $data['URLReturn'] = $returnUrl . '?mode=return';
        $this->validateRequest($data);
        \Log::info(print_r($data, true));
        $result = $this->merchant->generateRequestURL($data);

        if ($result['Errno'] != 0) {
            \Log::info('Neosurf generateRequestURL error '.$result['Errno']);
            throw new \Exception(
                'Temporary problem with payment provider.
                Please try again later or select different payment provider.'
            ); // todo translation
        }

        return $result['RedirectURL'];
    }

    /**
     * @param $data
     *
     * @throws \Exception
     */
    private function validateRequest($data)
    {
        $rules = [
            'IDMerchant' => 'required',
            'IDService' => 'required',
            'URLConfirmation' => 'required',
            'IDTransaction' => 'required',
            'amount' => 'required',
            'Type' => 'required',
            'URLReturn' => 'required',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            throw new \Exception('Missing fields in API request. Contact administrator.');
        }
    }
}