<?php namespace Keios\PGNeosurf\Classes;

/**
 * Class NeosurfConf
 * @package Keios\PGNeosurf\Classes
 */
class NeosurfConf
{

    /**
     * @return string
     */
    public function getServiceUrl()
    {
        return "https://www.neosurf.info/public/en/purchase.php";
    }


    /**
     * @return array
     */
    public function getGeneralErrors()
    {
        return [
            'InvalidRequestKey'     => 1000,
            'InvalidResponseKey'    => 2000,
            'HashError'             => 500,
            'InvalidDataFormat'     => 1001,
            'RequestMechantIdError' => 1002,
        ];
    }

    /**
     * @return array
     */
    public function getRequiredVars()
    {
        return [
            'IDMerchant',
            'IDService',
            'amount',
            'Type',
            'IDTransaction',
            'URLReturn',
            'URLConfirmation',
        ];
    }

    /**
     * @return array
     */
    public function getRequiredVarsMaxLen()
    {
        return [
            'IDMerchant'      => 6,
            'IDService'       => 4,
            'amount'          => 7,
            'Type'            => 1000,
            'IDTransaction'   => 255,
            'URLReturn'       => 255,
            'URLConfirmation' => 255,
        ];
    }


    /**
     * @return array
     */
    public function getRequiredVarsMaxLenErrorNo()
    {
        return [
            'IDMerchant'      => 1020,
            'IDService'       => 1021,
            'amount'          => 1022,
            'Type'            => 1023,
            'IDTransaction'   => 1024,
            'URLReturn'       => 1025,
            'URLConfirmation' => 1026,
        ];

    }

    /**
     * @return array
     */
    public function getRequiredVarsTypes()
    {
        return [
            'IDMerchant'      => 'N',
            'IDService'       => 'N',
            'amount'          => 'N',
            'Type'            => 'A',
            'IDTransaction'   => 'A',
            'URLReturn'       => 'A',
            'URLConfirmation' => 'A',
        ];
    }

    /**
     * @return array
     */
    public function getRequiredVarsTypesErrorNo()
    {
        return [
            'IDMerchant'      => 1040,
            'IDService'       => 1041,
            'amount'          => 1042,
            'Type'            => 1043,
            'IDTransaction'   => 1044,
            'URLReturn'       => 1045,
            'URLConfirmation' => 1046,
        ];

    }

    /**
     * @return array
     */
    public function getMissingRequiredVarsErrorNo()
    {
        return [
            'IDMerchant'      => 1060,
            'IDService'       => 1061,
            'amount'          => 1062,
            'Type'            => 1063,
            'IDTransaction'   => 1064,
            'URLReturn'       => 1065,
            'URLConfirmation' => 1066,
        ];
    }

    /**
     * @return array
     */
    public function getResVars()
    {
        return ['IDTransaction', 'ReponseNeosurf'];
    }

    /**
     * @return array
     */
    public function getResVarsMaxLen()
    {
        return [
            'IDTransaction'  => 255,
            'ReponseNeosurf' => 2,
        ];
    }

    /**
     * @return array
     */
    public function getResVarsMaxLenErrorNo()
    {
        return [
            'IDTransaction'  => 2020,
            'ReponseNeosurf' => 2021,
        ];
    }

    /**
     * @return array
     */
    public function getResVarsTypes()
    {
        return [
            'IDTransaction'  => 'A',
            'ReponseNeosurf' => 'N',
        ];

    }

    /**
     * @return array
     */
    public function getResVarsTypesErrorNo()
    {
        return [
            'IDTransaction'  => 2040,
            'ReponseNeosurf' => 2041,
        ];
    }

    /**
     * @return array
     */
    public function getMissingResVarsErrorNo()
    {
        return [
            'IDTransaction'  => 2060,
            'ReponseNeosurf' => 2061,
        ];
    }

}