<?php namespace Keios\PGNeosurf\Classes;

/**
 * Class NeosurfMerchantReceive
 * @package Keios\PGNeosurf\Classes
 */
class NeosurfMerchantReceive
{

    /**
     * @var NeosurfUtils
     */
    private $merchant;

    /**
     * NeosurfMerchantReceive constructor.
     */
    public function __construct()
    {
        $this->merchant = new NeosurfMerchant();
    }

    /**
     * Parses the response, returns true if transaction went fine or false if it didn't
     *
     * @return bool
     */
    public function receive($receivedArray)
    {

        //$data = $_POST['rep'];
        $data = $receivedArray['rep'];

        if (get_magic_quotes_gpc()) {
            $data = stripslashes($data);
        }

        $result = $this->merchant->parseResponse($data);


        if ($result['Errno'] != 0) {
            \Log::info('Neosurf parseResponse error '.$result['Errno']);
        } else {
            /*
             * From now you can use $trsdata['IDTransaction'] to get the corresponding transaction on your side
             */

            if (1 == $result['ReponseNeosurf']) {
                return true;
            } else {
                return false;
            }

        }
    }
}