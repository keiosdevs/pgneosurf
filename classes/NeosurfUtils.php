<?php namespace Keios\PGNeosurf\Classes;

/**
 * Class NeosurfUtils
 * @package Keios\PGNeosurf\Classes
 */
class NeosurfUtils
{
    /**
     * @param $trsdata
     * @param $nbparams
     * @param $hash
     *
     * @return mixed
     */
    public function generateDataString($trsdata, $nbparams, $hash)
    {
        $data = "";

        $data .= sprintf("%02d", $nbparams);

        $fieldsLength = "040";

        foreach ($trsdata as $val) {
            $fieldsLength .= sprintf("%03d", strlen($val));
        }

        $data .= $fieldsLength;

        $fieldsValues = $hash;

        foreach ($trsdata as $val) {
            $fieldsValues .= $val;
        }

        $data .= $fieldsValues;

        $scrumbledData = $this->scrumbleData($data);

        return $scrumbledData;
    }

    /**
     * @param $sid
     * @param $varDefArray
     *
     * @return null
     */
    public function parseDataString($sid, $varDefArray)
    {
        $config = new NeosurfConf();

        $neosurfGeneralErrors = $config->getGeneralErrors();
        $tmpArray = null;
        $tmpArray['Errno'] = 0;

        $data = $this->unscrumbleData($sid);

        $nbparams = substr($data, 0, 2);

        if ($nbparams < 2) {
            $tmpArray['Errno'] = $neosurfGeneralErrors['InvalidDataFormat'];

            return $tmpArray;
        }

        $startindex = (2 + $nbparams * 3);
        $fieldsValue = substr($data, $startindex);

        $tmpTotalLength = 0;

        for ($i = 0; $i < $nbparams; $i++) {
            $tmpFieldLength = substr($data, $i * 3 + 2, 3);
            $tmpFieldValue = substr($fieldsValue, $tmpTotalLength, $tmpFieldLength);
            $tmpTotalLength += $tmpFieldLength;

            if ($i == 0) {
                $tmpArray['merchantHash'] = $tmpFieldValue;
            } else {
                $tmpArray[$varDefArray[$i - 1]] = $tmpFieldValue;
            }
        }

        return $tmpArray;
    }

    /**
     * @param $trsdata
     * @param $merchantKey
     * @param $merchantHash
     *
     * @return int
     */
    public function checkHash($trsdata, $merchantKey, $merchantHash)
    {
        $config = new NeosurfConf();

        $neosurfGeneralErrors = $config->getGeneralErrors();
        $errno = 0;

        ksort($trsdata);
        reset($trsdata);
        $tohash = "";

        foreach ($trsdata as $key => $val) {
            $tohash .= $val;
        }

        $tohash .= $merchantKey;

        $serverHash = sha1($tohash);

        if (strcmp($serverHash, $merchantHash)) {
            $errno = $neosurfGeneralErrors['HashError'];
        }

        return $errno;
    }

    /**
     * @param $trsdata
     * @param $key
     *
     * @return string
     */
    public function computeHash($trsdata, $key)
    {
        ksort($trsdata);
        reset($trsdata);

        $tohash = "";
        foreach ($trsdata as $val) {
            $tohash .= $val;
        }

        $tohash .= $key;
        $hashed = sha1($tohash);

        return $hashed;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function scrumbleData($data)
    {
        $newData = "";
        for ($i = 0; $i < strlen($data); $i++) {
            $newData .= chr(ord($data[$i]) + 5);
        }

        return $newData;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function unscrumbleData($data)
    {
        $newData = "";

        for ($i = 0; $i < strlen($data); $i++) {
            $newData .= chr(ord($data[$i]) - 5);
        }

        return $newData;
    }

    /**
     * @param $inarr
     *
     * @return int
     */
    public function checkRequestArray($inarr)
    {
        $errno = 0;

        $errno = $this->checkRequestArrayVarNames($inarr);

        if ($errno != 0) {
            return $errno;
        }

        $errno = $this->checkRequestArrayVarLen($inarr);

        if ($errno != 0) {
            return $errno;
        }

        $errno = $this->checkRequestArrayVarTypes($inarr);
        if ($errno != 0) {
            return $errno;
        }
    }

    /**
     * @param $inarr
     *
     * @return int
     */
    public function checkRequestArrayVarNames($inarr)
    {
        $config = new NeosurfConf();
        $utils = new NeosurfUtils();
        $errno = 0;

        $requiredVars = $config->getRequiredVars();
        $missingVarsErrorNo = $config->getMissingRequiredVarsErrorNo();
        $generalErrors = $config->getGeneralErrors();

        foreach ($inarr as $key => $val) {
            if (!in_array($key, $requiredVars)) {
                $errno = $generalErrors['InvalidRequestKey'];
                break;
            }
        }

        foreach ($requiredVars as $val) {
            if (!array_key_exists($val, $inarr)) {
                $errno = $missingVarsErrorNo[$val];
                break;
            }
        }

        return $errno;
    }

    /**
     * @param $inarr
     *
     * @return int
     */
    public function checkRequestArrayVarLen($inarr)
    {
        $config = new NeosurfConf();
        $requiredVarsMaxLen = $config->getRequiredVarsMaxLen();
        $requiredVarsMaxLenErrorNo = $config->getRequiredVarsMaxLenErrorNo();

        $errno = 0;

        foreach ($inarr as $key => $val) {
            if (strlen($val) > $requiredVarsMaxLen[$key]) {
                $errno = $requiredVarsMaxLenErrorNo[$key];
                break;
            }
        }

        return $errno;
    }

    /**
     * @param $inarr
     *
     * @return int
     */
    public function checkRequestArrayVarTypes($inarr)
    {
        $config = new NeosurfConf();
        $errno = 0;

        $requiredVarsTypes = $config->getRequiredVarsTypes();

        foreach ($inarr as $key => $val) {
            $tmperrno = $this->checkRequestType($key, $val, $requiredVarsTypes[$key]);
            if ($tmperrno != 0) {
                $errno = $tmperrno;
                break;
            }
        }

        return $errno;
    }

    /**
     * @param $key
     * @param $val
     * @param $type
     *
     * @return int
     */
    public function checkRequestType($key, $val, $type)
    {
        $config = new NeosurfConf();
        $requiredVarsTypesErrorNo = $config->getRequiredVarsTypesErrorNo();

        return ((($type == 'N') && (!is_numeric($val))) || (($type == 'A') && (!is_string($val)))) ?
            $requiredVarsTypesErrorNo[$key] : 0;
    }

    /**
     * @param $inarr
     *
     * @return int
     */
    public function checkResponseArray($inarr)
    {
        $errno = 0;

        $errno = $this->checkResponseArrayVarNames($inarr);
        if ($errno != 0) {
            return $errno;
        }

        $errno = $this->checkResponseArrayVarLen($inarr);
        if ($errno != 0) {
            return $errno;
        }

        $errno = $this->checkResponseArrayVarTypes($inarr);
        if ($errno != 0) {
            return $errno;
        }
    }

    /**
     * @param $inarr
     *
     * @return int
     */
    public function checkResponseArrayVarNames($inarr)
    {
        $config = new NeosurfConf();

        $resVars = $config->getResVars();
        $missingResVarsErrorNo = $config->getMissingResVarsErrorNo();
        $generalErrors = $config->getGeneralErrors();

        $errno = 0;

        foreach ($inarr as $key => $val) {
            if (!in_array($key, $resVars)) {
                $errno = $generalErrors['InvalidResponseKey'];
                break;
            }
        }

        foreach ($resVars as $val) {
            if (!array_key_exists($val, $inarr)) {
                $errno = $missingResVarsErrorNo[$val];
                break;
            }
        }

        return $errno;
    }

    /**
     * @param $inarr
     *
     * @return int
     */
    public function checkResponseArrayVarLen($inarr)
    {
        $config = new NeosurfConf();

        $resVarsMaxLen = $config->getResVarsMaxLen();
        $resVarsMaxLenErrorNo = $config->getResVarsMaxLenErrorNo();

        $errno = 0;
        foreach ($inarr as $key => $val) {
            if (strlen($val) > $resVarsMaxLen[$key]) {
                $errno = $resVarsMaxLenErrorNo[$key];
                break;
            }
        }

        return $errno;
    }

    /**
     * @param $inarr
     *
     * @return int
     */
    public function checkResponseArrayVarTypes($inarr)
    {
        $config = new NeosurfConf();
        $utils = new NeosurfUtils();

        $resVarsTypes = $config->getResVarsTypes();
        $errno = 0;

        foreach ($inarr as $key => $val) {
            $tmperrno = $utils->checkResponseType($key, $val, $resVarsTypes[$key]);
            if ($tmperrno != 0) {
                $errno = $tmperrno;
                break;
            }
        }

        return $errno;
    }

    /**
     * @param $key
     * @param $val
     * @param $type
     *
     * @return int
     */
    public function checkResponseType($key, $val, $type)
    {
        $config = new NeosurfConf();

        $resVarsTypesErrorNo = $config->getResVarsTypesErrorNo();

        return ((($type == 'N') && (!is_numeric($val))) || (($type == 'A') && (!is_string($val)))) ?
            $resVarsTypesErrorNo[$key] : 0;
    }

}