<?php namespace Keios\PGNeosurf\Classes;

use Keios\PaymentGateway\Traits\SettingsDependent;

/**
 * Class NeosurfMerchant
 * @package Keios\PGNeosurf\Classes
 */
class NeosurfMerchant
{
    use SettingsDependent;

    /**
     * @var NeosurfConf
     */
    private $config;

    /**
     * @var NeosurfUtils
     */
    private $utils;

    /**
     * NeosurfMerchant constructor.
     */
    public function __construct()
    {
        $this->config = new NeosurfConf();
        $this->utils = new NeosurfUtils();
    }

    /**
     * @param $requestData
     *
     * @return null
     */
    public function generateRequestURL($requestData)
    {
        $this->getSettings();

        $neosurfServiceUrl = $this->config->getServiceUrl();
        $neosurfRequiredVars = $this->config->getRequiredVars();
        // $neosurfMerchantKey = $this->getSettings()->get('neosurf.merchantId'); // todo remove
        $neosurfSecretKey = $this->getSettings()->get('neosurf.apiKey');

        // defaults
        $data = "";
        $errNo = 0;
        $neosurfURL = "";
        $result = null;

        if (count($requestData) >= 1) {
            ksort($requestData);
            reset($requestData);
        }

        $errNo = $this->utils->checkRequestArray($requestData);

        if ($errNo != 0) {
            $result['Errno'] = $errNo;

            return $result;
        }

        $params = count($neosurfRequiredVars) + 1;

        $merchantHash = $this->utils->computeHash($requestData, $neosurfSecretKey);

        $data = $this->utils->generateDataString($requestData, $params, $merchantHash);

        $neosurfURL = $neosurfServiceUrl."?sid=".urlencode($data);

        $neosurfURL .= "&IDMerchant=".$requestData['IDMerchant'];

        $result['Errno'] = 0;
        $result['RedirectURL'] = $neosurfURL;

        return $result;
    }

    /**
     * @param $sid
     *
     * @return null
     */
    public function parseResponse($sid)
    {
        $this->getSettings();

        //$neosurfResVars = $this->config->getRequiredVars();
        $neosurfResVars = array('IDTransaction', 'ReponseNeosurf');
        $neosurfMerchantKey = $this->getSettings()->get('neosurf.apiKey');

        sort($neosurfResVars);
        reset($neosurfResVars);

        $requestData = null;

        $requestData = $this->utils->parseDataString($sid, $neosurfResVars);
        if ($requestData['Errno'] != 0) {
            return $requestData;
        }

        $merchantHash = $requestData['merchantHash'];
        $newTrsData = $requestData;
        $requestData = null;
        foreach ($newTrsData as $key => $val) {
            if (strcmp($key, 'merchantHash') && strcmp($key, 'Errno')) {
                $requestData[$key] = $val;
            }
        }

        $errorNumber = $this->utils->checkHash($requestData, $neosurfMerchantKey, $merchantHash);

        if ($errorNumber != 0) {
            $requestData = null;
            $requestData['Errno'] = $errorNumber;

            return $requestData;
        }

        $requestData['Errno'] = 0;

        return $requestData;
    }

}